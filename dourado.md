---
layout: home

estalajadeiro:
  exibir: true
  balao:
    titulo: Vem pra cá!
    texto: ''
    link:
      texto: Inscreva-se ou conecte-se agora.
      href: /login
  animacoes:
    sources:
      - src: /assets/video/estalajadeiro.webm
        type: video/webm
        legendas: ""
    audiodescricao:
      src: ""
      label: ""

slides:
  - inicial: true
    titulo: Entre com tudo no Formato Clássico
    descricao: "Adicione pacotes Clássicos ou Dourados e cards Lendários aleatórios à sua Coleção para o Livre e para o novo formato Clássico, onde você pode reviver o passado de Hearthstone com cards e combos ilustres!"
    imagem:
      src: /assets/img/slides/slide1.jpg
      alt: lorem ipsum
    indicador:
      cooldown: dourado
      imagem: 
        src: /assets/img/indicadores/indicador1.png
        alt: lorem ipsum
    botoes:
      - texto: comprar clássico
        href: /download
        target: _blank
        rel: noopener
        cor: dourado
        tamanho: small
      - texto: Comprar dourado
        href: /download
        target: _blank
        rel: noopener
        cor: dourado
        tamanho: small
  - titulo: Um presente dourado!
    descricao: "Para celebrar o lançamento do formato Clássico, daremos a todos os jogadores um Pacote Dourado de cards Clássicos quando se conectarem após o lançamento do patch 20.0! Limite de um por conta."
    imagem:
      src: /assets/img/slides/slide2.jpg
      alt: lorem ipsum
    indicador:
      cooldown: dourado
      imagem: 
        src: /assets/img/indicadores/indicador2.png
        alt: lorem ipsum
    botoes:
      - texto: saiba mais
        href: /download
        target: _blank
        rel: noopener
        cor: dourado
        tamanho: small
  - titulo: Prepare-se para os Sertões com o Pacotaço
    descricao: "Compre o Pacotaço de Forjado nos Sertões na pré-venda para melhorar sua coleção com pacotes de cards Dourados, itens cosméticos, bônus dos Campos de Batalha e muito mais!"
    imagem:
      src: /assets/img/slides/slide3.jpg
      alt: lorem ipsum
    indicador:
      cooldown: dourado
      imagem: 
        src: /assets/img/indicadores/indicador3.png
        alt: lorem ipsum
    botoes:
      - texto: Compre já
        href: /download
        target: _blank
        rel: noopener
        cor: dourado
        tamanho: small
      - texto: saiba mais
        href: /download
        target: _blank
        rel: noopener
        cor: dourado
        tamanho: small
  - titulo: Forjado nos Sertões – Já disponível!
    descricao: "Teste sua bravura em condições árduas e desafios avassaladores ao se juntar a nós na mais nova expansão de Hearthstone, Forjado nos Sertões — já disponível!"
    imagem:
      src: /assets/img/slides/slide4.jpg
      alt: lorem ipsum
    indicador:
      cooldown: dourado
      imagem: 
        src: /assets/img/indicadores/indicador4.png
        alt: lorem ipsum
    botoes:
      - texto: Compre já
        href: /download
        target: _blank
        rel: noopener
        cor: dourado
        tamanho: small
      - texto: saiba mais
        href: /download
        target: _blank
        rel: noopener
        cor: dourado
        tamanho: small
  - titulo: Ano do Grifo
    descricao: "O novo ano de Hearthstone trará mais expansões, eventos sazonais, atualizações para os Campos de batalha, o novo modo Mercenários e muito mais!"
    imagem:
      src: /assets/img/slides/slide5.jpg
      alt: lorem ipsum
    indicador:
      cooldown: dourado
      imagem: 
        src: /assets/img/indicadores/indicador5.png
        alt: lorem ipsum
    botoes:
      - texto: saiba mais
        href: /download
        target: _blank
        rel: noopener
        cor: dourado
        tamanho: small
---
