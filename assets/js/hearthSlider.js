class HearthSlider {

  carousel;

  config = {
    interval: 6000,
    pause: false
  }

  selector;

  _stateControl;

  constructor(selector, config = this.config) {

    config = {

      interval: this.config.interval,
      pause: this.config.pause,

      ...config
    }

    this.config = config;
    
    // configura carousel Bootstrap
    this.selector = selector;
    this.carousel = new bootstrap.Carousel(this.nodeElement, config);

    this._stateControl = this.nodeElement.querySelector("#stateControl");

    // permite configurar a duração da animação de cooldown via JS
    window.onload = () => { this._addStyles(this._cooldownStyles) };

    this.nodeElement.addEventListener('slide.bs.carousel', this._transitionHandler);
    this._stateControl.addEventListener('click', this._stateHandler)
  }

  get nodeElement() {

    const selector = this.selector;

    return document.querySelector(selector)
  }

  get state() {

    return this.nodeElement.getAttribute('data-bs-state');
  }
  
  get _cooldownStyles() {

    const styles = `
    .indicator__cooldown.active .cooldown__circle:not(anim--pause) {
  
      animation: anim-cooldown--clockwise;
      animation-duration: ${this.config.interval}ms;
      animation-fill-mode: forwards;
    }`;

    return styles
  }
  
  _addStyles(styles) {
          
    const css = document.createElement('style');
  
    if (css.styleSheet) css.styleSheet.cssText = styles;
    else css.appendChild(document.createTextNode(styles));
      
    document.head.appendChild(css);
  }

  // transiciona de acordo com a direção do slider
  _transitionHandler = (event) => {
  
    const { 
      direction,
      relatedTarget
    } = event;
    
    const image = relatedTarget.querySelector('img');
  
    switch (direction) {
      case 'right':
        image.classList.remove('anim__slide--left');
        image.classList.add('anim__slide--right');
        break;
    
      default:
        image.classList.remove('anim__slide--right');
        image.classList.add('anim__slide--left');
        break;
    }
  }

  // controle de estado
  _stateHandler = () => {

    const stateControl = {
      pause : this._stateControl.querySelector("#stateControlPause"),
      play  : this._stateControl.querySelector("#stateControlPlay"),
      texto : this._stateControl.querySelector(".hs__state-text")
    }

    const cooldownCircles = 
      [...this.nodeElement.querySelectorAll('.indicator__cooldown .cooldown__circle')];

    const pauseCooldown = () => {

      cooldownCircles.forEach(circle => { circle.classList.toggle('anim--pause') });
    };

    const toggleControlIcon = () => {

      stateControl.pause.classList.toggle('d-none');
      stateControl.play.classList.toggle('d-none');
    };
  
    switch (this.state) {
      case 'pause':
        toggleControlIcon();
        pauseCooldown();

        this.carousel.cycle();
        this.nodeElement.setAttribute('data-bs-state', 'play');
  
        stateControl.texto.innerHTML = "pausar";
        break;
  
      default:
        toggleControlIcon();
        pauseCooldown()

        this.carousel.pause();
        this.nodeElement.setAttribute('data-bs-state', 'pause');
        
        stateControl.texto.innerHTML = "continuar";
        ;
        break;
    }
  }
}
